/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shit_rt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmazurok <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 14:31:48 by vmazurok          #+#    #+#             */
/*   Updated: 2018/04/27 14:31:49 by vmazurok         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "gfx.h"
// VECTOR_-----------------------
typedef struct	s_vector
{
	double x;
	double y;
	double z;
}				t_vector;



// RAY_--------------------------
typedef struct	s_ray
{
	t_vector origin;
	t_vector direct;
}				t_ray;




// CAM_--------------------------
typedef struct	s_camera
{
	t_vector	origin;
	t_vector	direct;
	double		dest;
}				t_camera;





t_vector	set_vector(double x, double y, double z)
{
	t_vector v;

	v.x = x;
	v.y = y;
	v.z = z;
	return (v);
}

void		pixel_add(t_grafx *gfx, int x, int y, unsigned int color)
{
	int bpp;
	int size_l;
	int end;
	int *buff;

	if (x >= 0 && x < gfx->scr_size && y >= 0 && y < gfx->scr_size)
	{
		buff = (int *)mlx_get_data_addr(gfx->img, &bpp, &size_l, &end);
		buff[(int)(gfx->scr_size) * y + x] = color;
	}
}

int			main(void)
{
	t_grafx *gfx;

	// Window--------------------------------
	gfx = (t_grafx *)malloc(sizeof(t_grafx));
	if (!(gfx->mlx = mlx_init()))
	{
		perror("Error");
		exit(1);
	}
	if (!(gfx->win = mlx_new_window(gfx->mlx, SCR_SIZE, SCR_SIZE, "rt")))
	{
		perror("Error");
		exit(1);
	}
	if (!(gfx->img = mlx_new_image(gfx->mlx, SCR_SIZE, SCR_SIZE)))
	{
		perror("Error");
		exit(1);
	}
	// Window------------------------------

	int i;
	int j;
	t_ray ray;
	t_camera camera;
	t_vector vec;

	camera.origin = set_vector(10, 10, -3);
	camera.direct = set_vector(0, -2, 0);
	camera.dest = 3;
	j = -1;
	ray.origin = camera.origin;
	while (++j < SCR_SIZE)
	{
		i = -1;
		while (++i < SCR_SIZE)
		{
			vec = add_vector(set_vector(), set_vector());
			ray.direct = set_vector()
			color = intersect();
		}
	}
	mlx_loop(gfx->mlx);
	return 0;
}

























