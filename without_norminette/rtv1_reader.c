/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shit_rt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmazurok <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 14:31:48 by vmazurok          #+#    #+#             */
/*   Updated: 2018/04/27 14:31:49 by vmazurok         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"


//==========================================================================

double		v_mod(t_vector v)
{
	return (sqrt(v.x * v.x + v.y * v.y + v.z * v.z));
}

t_vector	set_vector(double x, double y, double z)
{
	t_vector v;

	v.x = x;
	v.y = y;
	v.z = z;
	return (v);
}

t_vector	v_to_len(t_vector v, double len, int norm)
{
	double mod;

	if (!norm)
	{
		mod = v_mod(v);
		v.x = v.x / mod * len;
		v.y = v.y / mod * len;
		v.z = v.z / mod * len;
	}
	else
	{
		v.x = v.x * len;
		v.y = v.y * len;
		v.z = v.z * len;	
	}
	return (v);
}

t_vector	add_vectors(t_vector v1, t_vector v2)
{
	t_vector sum;

	sum.x = v1.x + v2.x;
	sum.y = v1.y + v2.y;
	sum.z = v1.z + v2.z;
	return (sum);
}

t_vector	sub_vectors(t_vector v1, t_vector v2)
{
	t_vector sub;

	sub.x = v1.x - v2.x;
	sub.y = v1.y - v2.y;
	sub.z = v1.z - v2.z;
	return (sub);
}

void		pixel_add(t_grafx *gfx, int x, int y, unsigned int color)
{
	int bpp;
	int size_l;
	int end;
	int *buff;

	if (x >= 0 && x < SCR_SIZE && y >= 0 && y < SCR_SIZE)
	{
		buff = (int *)mlx_get_data_addr(gfx->img, &bpp, &size_l, &end);
		buff[SCR_SIZE * y + x] = color;
	}
}

double		scalar_dob(t_vector v1, t_vector v2)
{
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}


int			lighter(t_vector nor, t_ray s_ray, t_light *light, t_vector h)
{
	int		col_b;
	int		col_g;
	int		col_r;
	double	nor_li;
	double	w;

	nor_li = scalar_dob(nor, s_ray.direct);
	w = fmax(0, scalar_dob(nor, h));
	w = pow(w, 12);

	col_b = fmin(0xFF, (light->color & 255) * fmax(0, nor_li) * light->intence
	+ (light->color & 255) * w * light->intence);
	col_g = fmin(0xFF, (light->color >> 8 & 255) * fmax(0, nor_li) * 
	light->intence + (light->color >> 8 & 255) * w * light->intence);
	col_r = fmin(0xFF, (light->color >> 16 & 255) * fmax(0, nor_li) *
	light->intence + (light->color >> 16 & 255) * w * light->intence);
	return (col_b + (col_g * 256) + (col_r * 256 * 256));
}

int			ambient_light(int shape_col, t_light *light)
{
	int		col_b;
	int		col_g;
	int		col_r;
	double	k;

	k = 0;
	while (light)
	{
		k = fmax(light->intence / 2, k);
		light = light->next;
	}
	col_b = fmax(0, (shape_col & 255) * k);
	col_g = fmax(0, (shape_col >> 8 & 255) * k);
	col_r = fmax(0, (shape_col >> 16 & 255) * k);
	return (col_b + (col_g * 256) + (col_r * 256 * 256));
}

t_vector	vec_dob(t_vector v1, t_vector v2)
{
	double		i;
	double		j;
	double		k;

	i = (v1.y * v2.z) - (v2.y * v1.z);
	j = -((v1.x * v2.z) - (v2.x * v1.z));
	k = (v1.x * v2.y) - (v2.x * v1.y);
	return(set_vector(i, j, k));
}

int			color_add(int first, int second)
{
	int sum;

	sum = fmin(0xFF, (first & 255) + (second & 255));
	sum += fmin(0xFF, (first >> 8 & 255) + (second >> 8 & 255)) * 256;
	sum += fmin(0xFF, (first >> 16 & 255) + (second >> 16 & 255)) * 256 * 256;
	return (sum);
}

int			not_in_the_shadow(t_ray s_ray, t_shape *sh, t_light *light)
{
	double b;

	while (sh)
	{
		if ((sh->id != s_ray.id) && (sh->inter(s_ray, sh->shape, light, &b, NULL)))
		{
			if ((!light->is_dir && b < v_mod(sub_vectors(s_ray.origin, light->direct))) ||
			light->is_dir)
				return (0);
		}
		sh = sh->next;
	}
	return (1);
}

int			light_calculate(t_vector nor, t_ray s_ray, t_light *light,
t_shape *sh)
{
	double		b;
	t_vector	h;
	int			color;

	color = ambient_light(s_ray.main_col, light);
	while (light)
	{
		if (light->is_dir)
			s_ray.direct = v_to_len(light->direct, -1, 0);
		else
			s_ray.direct = v_to_len(sub_vectors(light->direct, s_ray.origin),
			1, 0);
		if (not_in_the_shadow(s_ray, sh, light))
		{
			h = add_vectors(s_ray.rev_dir, s_ray.direct);
			color = color_add(color, lighter(nor, s_ray, light, v_to_len(h,
			1, 0)));
		}
		light = light->next;
	}
	return (color);
}


double		intersect_sphere(t_ray ray, void *sf, t_light *light, double *t, t_shape *sh)
{
	t_vector	v1;
	t_vector	v2;
	t_vector	nor;
	t_vector	li;
	t_vector	minus;
	int			color;
	double		descr;
	double		t0;
	double		t1;
	double		a;
	double		b;
	double		c;

	t_sphere *s = (t_sphere *)sf;

	v1 = sub_vectors(ray.origin, s->centre);
	a = scalar_dob(ray.direct, ray.direct);
	b = 2 * scalar_dob(ray.direct, v1);
	c = scalar_dob(v1, v1) - s->radius * s->radius;
	descr = b * b - 4 * a * c;
	if (descr <= 0)
	{
		*t = 1000500;
		return (0);
	}
	descr = sqrt(descr);
	t0 = (-b - descr) / (2 * a);
	t1 = (-b + descr) / (2 * a);
	*t = (t0 < t1) ? t0 : t1;
	if (*t < 0)
	{
		*t = 1000500;
		return (0);
	}
	if (sh)
	{
		t_ray s_ray;
		s_ray.origin = add_vectors(ray.origin, v_to_len(ray.direct, *t, 0));
		s_ray.id = s->id;
		s_ray.main_col = s->color;
		s_ray.rev_dir = v_to_len(ray.direct, -1, 1);
		nor = v_to_len(sub_vectors(s_ray.origin, s->centre), 1, 0);
		return (light_calculate(nor, s_ray, light, sh));
	}
	else
		return (1);
}

double		intersect_plane(t_ray ray, void *pl, t_light *light, double *t, t_shape *sh)
{
	t_vector	nor;
	int			color;
	double		znam;
	t_ray		s_ray;
	t_plane		*p;

	p = (t_plane *)pl;
	if ((znam = scalar_dob(ray.direct, p->normal)))
	{
		nor = sub_vectors(ray.origin, p->point);
		*t = (-1) * scalar_dob(nor, p->normal) / znam;
		if (*t > 0)
		{
			if (sh)
			{
				nor = v_to_len(p->normal, -1, 0);
				s_ray.id = p->id;
				s_ray.main_col = p->color;
				s_ray.rev_dir = v_to_len(ray.direct, -1, 1);
				s_ray.origin = add_vectors(ray.origin, v_to_len(ray.direct, *t, 0));
				return (light_calculate(nor, s_ray, light, sh));
			}
			else
				return (1);
		}
	}
	*t = 1000500;
	return (0);
}

double		intersect_cylinder(t_ray ray, void *cy, t_light *light, double *t, t_shape *sh)
{
	t_vector	x;
	t_vector	nor;
	t_vector	li;
	int			color;
	double		descr;
	double		t0;
	double		t1;
	double		a;
	double		b;
	double		c;
	// double		t;

	t_cylinder *cyl = (t_cylinder *)cy;
	x = sub_vectors(ray.origin, cyl->point);
	t0 = scalar_dob(ray.direct, cyl->direct);
	t1 = scalar_dob(x, cyl->direct);

	a = scalar_dob(ray.direct, ray.direct) - (t0 * t0);
	b = 2 * (scalar_dob(ray.direct, x) - t0 * t1);
	c = scalar_dob(x, x) - (t1 * t1) - cyl->radius * cyl->radius;
	
	descr = b * b - 4 * a * c;
	if (descr < 0)
	{
		*t = 1000500;
		return (0);
	}
	descr = sqrt(descr);
	t0 = (-b - descr) / (2 * a);
	t1 = (-b + descr) / (2 * a);
	*t = (t0 < t1) ? t0 : t1;
	if (*t > 0)
	{
		if (sh)
		{
			t_ray s_ray;
			s_ray.origin = add_vectors(ray.origin, v_to_len(ray.direct, *t, 0));
			s_ray.id = cyl->id;
			s_ray.main_col = cyl->color;
			s_ray.rev_dir = v_to_len(ray.direct, -1, 1);
			x = add_vectors(ray.origin, v_to_len(ray.direct, *t, 0));
			a = scalar_dob(ray.direct, cyl->direct) * (*t) + scalar_dob(x, cyl->direct);
			li = sub_vectors(x, cyl->point);
			nor = v_to_len(sub_vectors(li, v_to_len(cyl->direct, a, 0)), 1, 0);
			return (light_calculate(nor, s_ray, light, sh));
		}
		else
			return (1);
	}
	*t = 1000500;
	return (0);
}

double			intersect_cone(t_ray ray, void *con, t_light *light, double *t, t_shape *sh)
{
	t_vector	v;
	t_vector	li;
	t_vector	nor;
	t_vector	x;
	t_vector	p;
	double		a;
	double		b;
	double		c;
	double		descr;
	double		t1;
	double		t0;
	int			color;

	t_cone *cone = (t_cone *)con;

	x = sub_vectors(ray.origin, cone->point);
	t0 = scalar_dob(ray.direct, cone->direct);
	t1 = scalar_dob(x, cone->direct);
	c = (1 + cone->ang * cone->ang);

	a = scalar_dob(ray.direct, ray.direct) - c * (t0 * t0);
	b = 2 * (scalar_dob(ray.direct, x) - c * t0 * t1);
	c = scalar_dob(x, x) - c * (t1 * t1);

	descr = b * b - 4 * a * c;
	if (descr < 0)
	{
		*t = 1000500;
		return (0);
	}
	descr = sqrt(descr);
	t0 = (-b - descr) / (2 * a);
	t1 = (-b + descr) / (2 * a);
	*t = (t0 < t1) ? t0 : t1;
	if (*t > 0)
	{
		if (sh)
		{
			t_ray s_ray;
			s_ray.origin = add_vectors(ray.origin, v_to_len(ray.direct, *t, 0));
			s_ray.id = cone->id;
			s_ray.main_col = cone->color;
			s_ray.rev_dir = v_to_len(ray.direct, -1, 1);
			a = scalar_dob(ray.direct, cone->direct) * (*t) + scalar_dob(x, cone->direct);
			a = (1 + cone->ang * cone->ang) * a;
			nor = v_to_len(sub_vectors(sub_vectors(s_ray.origin, cone->point), v_to_len(cone->direct, a, 0)), 1, 0);
			return (light_calculate(nor, s_ray, light, sh));
		}
		else
			return (1);
	}
	*t = 1000500;
	return (0);
}

void		add_shape(t_shape **sh, void *shape, char name, int id)
{
	t_shape 	*step;
	t_shape 	*last;

	last = (t_shape *)malloc(sizeof(t_shape));
	last->name = name;
	last->id = id;
	last->shape = shape;
	if (name == 's')					// sphere
		last->inter = &intersect_sphere;
	else if (name == 'p')				// plane
		last->inter = &intersect_plane;
	else if (name == 'c')				// cone
		last->inter = &intersect_cone;
	else								// cylinder
		last->inter = &intersect_cylinder;
	last->next = NULL;
	step = *sh;
	if (step)
	{
		while (step->next)
			step = step->next;
		step->next = last;
	}
	else
		*sh = last;
}		

void		drowing(t_grafx *gfx, t_shape *shape_list, t_light *light, t_camera camera)
{
	double i;
	double j;
	int color;
	t_ray ray;
	t_vector direction;
	camera.direct = v_to_len(camera.direct, 1, 0);
	if (camera.direct.x == 0 && camera.direct.z == 0)
	{
		camera.up = set_vector(1, 0, 0);
		camera.right = set_vector(0, 1, 0);
	}
	else
	{
		camera.up = set_vector(0, -1, 0);
		camera.right = vec_dob(camera.direct, camera.up);
		camera.right = v_to_len(camera.right, 1, 0);
		camera.up = vec_dob(camera.right, camera.direct);
	}
	// camera.dest = 3;

	/*//--------------------------------------------------------------------------------------_OUTPUT
	t_light *light_head;
	t_shape *shapes_head;
	light_head = light;
	shapes_head = shape_list;
	printf("________________ camera ________________ \ndir - (%.1f, %.1f, %.1f)\ncen - (%.1f, %.1f, %.1f)\n\n",
	camera.direct.x, camera.direct.y, camera.direct.z, camera.origin.x, camera.origin.y,
	camera.origin.z);
	if (!light)
		ft_putendl("You have no light sorces, ma dudes");
	if (light)
		printf("________________ lights ________________ \n");
	while (light)
	{
		printf("is_dir - %i, dir - (%.1f, %.1f, %.1f)\ncol - %i\nintence - %.2f\n\n", 
		light->is_dir, light->direct.x, light->direct.y, light->direct.z,
		light->color, light->intence);
		light = light->next;
	}
	if (shape_list)
		printf("________________  shapes ________________ \n");
	while (shape_list)
	{
		if (shape_list->name == 's')
			printf(">>sphere:\nrad -- %.1f\ncol -- %i\ncen - (%.1f, %.1f, %.1f)\n\n",
			((t_sphere *)(shape_list->shape))->radius, ((t_sphere *)(shape_list->shape))->color,
			((t_sphere *)(shape_list->shape))->centre.x, ((t_sphere *)(shape_list->shape))->centre.y,
			((t_sphere *)(shape_list->shape))->centre.z);
		else if (shape_list->name == 'p')
			printf(">>plane:\ncen - (%.1f, %.1f, %.1f)\nnor - (%.1f, %.1f, %.1f)\n color - %#X\n\n",
			((t_plane *)(shape_list->shape))->point.x, ((t_plane *)(shape_list->shape))->point.y,
			((t_plane *)(shape_list->shape))->point.z, ((t_plane *)(shape_list->shape))->normal.x,
			((t_plane *)(shape_list->shape))->normal.y, ((t_plane *)(shape_list->shape))->normal.z,
			((t_plane *)(shape_list->shape))->color);
		else if (shape_list->name == 'c')
			printf(">>cone:\ncen - (%.1f, %.1f, %.1f)\nang - %.1f\ndir - (%.1f, %.1f, %.1f)\n color - %#X\n\n",
			((t_cone *)(shape_list->shape))->point.x, ((t_cone *)(shape_list->shape))->point.y,
			((t_cone *)(shape_list->shape))->point.z, ((t_cone *)(shape_list->shape))->ang,
			((t_cone *)(shape_list->shape))->direct.x, ((t_cone *)(shape_list->shape))->direct.y,
			((t_cone *)(shape_list->shape))->direct.z, ((t_cone *)(shape_list->shape))->color);
		else if (!shape_list->name)
			printf(">>cylinder:\ncen - (%.1f, %.1f, %.1f)\nrad - %.1f\ndir - (%.1f, %.1f, %.1f)\n color - %#x\n",
			((t_cylinder *)(shape_list->shape))->point.x, ((t_cylinder *)(shape_list->shape))->point.y,
			((t_cylinder *)(shape_list->shape))->point.z, ((t_cylinder *)(shape_list->shape))->radius,
			((t_cylinder *)(shape_list->shape))->direct.x, ((t_cylinder *)(shape_list->shape))->direct.y,
			((t_cylinder *)(shape_list->shape))->direct.z, ((t_cone *)(shape_list->shape))->color);
		shape_list = shape_list->next;
	}
	light = light_head;
	shape_list = shapes_head;
	//--------------------------------------------------------------------------------------------*/


	// ray_casting===========================================

	t_vector up;
	t_vector right;
	t_vector dir;
	t_vector c;

	j = (-1) * SCR_SIZE / 2;
	t_shape *head;

	head = shape_list;
	ray.direct = v_to_len(camera.direct, 1, 0);
	while (j < SCR_SIZE / 2)
	{
		i = (-1) * SCR_SIZE / 2;
		while (i < SCR_SIZE / 2)
		{
			up = v_to_len(camera.up, j, 1);
			right = v_to_len(camera.right, i, 1);
			c = add_vectors(up, right);
			// dir = v_to_len(camera.direct, sqrt((SCR_SIZE / 2) * (SCR_SIZE / 2) - v_mod(c) * v_mod(c)), 0);
			ray.origin = add_vectors(c, camera.origin);
		
			double	min;
			double	dist;
			int		color;
			int		color_to_scene = 0x000000;
			min = 1000500;
			while (shape_list)
			{
				color = shape_list->inter(ray, shape_list->shape, light, &dist, head);
				if (dist <= min)
				{
					min = dist;
					color_to_scene = color;
				}
				shape_list = shape_list->next;
			}
			pixel_add(gfx, i + 500, j + 500, color_to_scene);
			shape_list = head;
			i++;
		}
		j++;
	}
	mlx_put_image_to_window(gfx->mlx, gfx->win, gfx->img, 0, 0);
}

/*
int			key_handle(int key, void *par)
{
	t_grafx *gfx;

	gfx = (t_grafx *)par;
	if (key == 124)
	{
		(gfx->x) += 0.1;
	}
	else if (key == 123)
	{
		(gfx->x) -= 0.1;
	}
	else if (key == 126)
	{
		(gfx->y) += 0.1;
	}
	else if (key == 125)
	{
		(gfx->y) -= 0.1;
	}
	drowing(gfx, gfx->shape_list);
	return (0);
}

int			ne_main(void)
{
	t_grafx *gfx;

	// Window--------------------------------
	gfx = (t_grafx *)malloc(sizeof(t_grafx));
	if (!(gfx->mlx = mlx_init()))
	{
		perror("Error");
		exit(1);
	}
	if (!(gfx->win = mlx_new_window(gfx->mlx, SCR_SIZE, SCR_SIZE, "shit_RTv1")))
	{
		perror("Error");
		exit(1);
	}
	if (!(gfx->img = mlx_new_image(gfx->mlx, SCR_SIZE, SCR_SIZE)))
	{
		perror("Error");
		exit(1);
	}
	//--------------------------------------------/
	gfx->x = 0;
	gfx->y = 0;

	int n = 0;

	// sphere params-----------------------------
	t_sphere sfer;
	sfer.centre = set_vector(0, 0, 0);
	sfer.radius = 100;
	sfer.color = 0x00ffff;
	sfer.id = n++;

	t_sphere sfer1;
	sfer1.centre = set_vector(400, 200, 100);
	sfer1.radius = 200;
	sfer1.id = n++;
	sfer1.color = 0xdd0000;
	//----------------------------------------------------

	// plane params-----------------------------
	t_plane p;
	p.normal = set_vector(-5, -30, 20);
	p.point = set_vector(0, -280, 0);
	p.color = 0x0000aa;
	p.id = n++;

	t_plane p1;
	p1.normal = set_vector(5, 10, 20);
	p1.point = set_vector(0, 0, 400);
	p1.color = 0x00F0aa;
	p1.id = n++;
	//------------------------------------------------------

	// cone params------------------------------------------
	t_cone cone;
	cone.direct = set_vector(-0.3, 1, 0);
	cone.point = set_vector(200, 0, 100);
	cone.color = 0x00ff00;
	cone.ang = 0.5;
	cone.direct = v_to_len(cone.direct, 1, 0);
	cone.id = n++;
	//----------------------------------------------------

	// cylinder params------------------------------------------
	t_cylinder cyl;
	cyl.direct = set_vector(0.5, 1, 0);
	cyl.point = set_vector(-300, 0, 0);
	cyl.color = 0xffff00;
	cyl.radius = 100;
	cyl.direct = v_to_len(cyl.direct, 1, 0);
	cyl.id = n++;
	//----------------------------------------------------



	// shapes-------------------------------------

	t_shape *shape_list = NULL;

	add_shape(&shape_list, &sfer, 's', sfer.id);
	add_shape(&shape_list, &sfer1, 's', sfer1.id);
	add_shape(&shape_list, &p1, 'p', p1.id);
	add_shape(&shape_list, &p, 'p', p.id);
	add_shape(&shape_list, &cone, 'c', cone.id);
	add_shape(&shape_list, &cyl, 0, cyl.id);

	//------------------------------------------
	gfx->shape_list = shape_list;
	drowing(gfx, gfx->shape_list);
	//========================================================


	// printf("cam.origin %f, %f, %f\n", camera.origin.x, camera.origin.y, camera.origin.z);
	// printf("cam.direct %f, %f, %f\n", camera.direct.x, camera.direct.y, camera.direct.z);
	mlx_hook(gfx->win, 2, (1L << 0), key_handle, (void *)gfx);
	mlx_loop(gfx->mlx);
	return (0);
}

*/























