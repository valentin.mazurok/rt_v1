/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfx.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmazurok <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 19:03:55 by vmazurok          #+#    #+#             */
/*   Updated: 2018/01/12 19:04:01 by vmazurok         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GFX_H
# define GFX_H
# include <mlx.h>
# include <fcntl.h>
# include <stdlib.h>
# include <math.h>
# include <unistd.h>
# include <pthread.h>
# include <stdio.h>
# include <errno.h>
# include "get_next_line.h"
# define SCR_SIZE 1000
# define OBJ_NUM 6

// VECTOR_-----------------------
typedef struct	s_vector
{
	double x;
	double y;
	double z;
}				t_vector;



// RAY_--------------------------
typedef struct	s_ray
{
	t_vector	origin;
	t_vector	direct;
	t_vector	rev_dir;
	int			id;
	int			main_col;
}				t_ray;


// CAM_--------------------------
typedef struct	s_camera
{
	t_vector	origin;
	t_vector	direct;
	t_vector	up;
	t_vector	right;
	int			is_set;
	double		dest;
}				t_camera;

// SPHERE_-----------------------
typedef struct	s_sphere
{
	int			color;
	t_vector	centre;
	int			id;
	double		radius;
}				t_sphere;

// LIGHT_--------------------------

typedef struct	s_light
{
	char			is_dir;
	t_vector		direct;
	int				color;
	double			intence;
	struct	s_light	*next;	
}				t_light;


// PLANE_-------------------------
typedef struct	s_plane
{
	t_vector	normal;
	t_vector	point;
	int			id;
	int			color;
}				t_plane;

// CONE_------------------------
typedef struct	s_cone
{
	t_vector	direct;
	t_vector	point;
	double		ang; // тангенс половини кута (0 - 1)
	int			id;
	int			color;
}				t_cone;


// CYLINDER_------------------------
typedef struct	s_cylinder
{
	t_vector	direct;
	t_vector	point;
	double		radius;
	int			id;
	int			color;
}				t_cylinder;


// SHAPES_-------------------------

typedef struct	s_shape
{
	char			name;
	int				id;
	void			*shape;
	double			(*inter)(t_ray, void *, t_light *, double *, struct	s_shape *);
	struct s_shape	*next;
}				t_shape;

typedef struct	s_obj
{
	char			name;
	void			*obj;
	struct s_obj	*next;
}				t_obj;

typedef struct	s_parce
{
	char		*name;
	int			(*parce)(int, int*, void**, int);
}				t_parce;


typedef struct	s_grafx
{
	void	*mlx;
	void	*win;
	void	*img;
	t_shape	*shape_list;
	double	x;
	double	y;
}				t_grafx;

void			add_shape(t_shape **sh, void *shape, char name, int id);
t_vector		set_vector(double x, double y, double z);
void			drowing(t_grafx *gfx, t_shape *shape_list, t_light *light, t_camera camera);
t_vector		v_to_len(t_vector v, double len, int norm);
double			scalar_dob(t_vector v1, t_vector v2);

#endif
