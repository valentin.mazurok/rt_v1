/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vmazurok <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/23 20:08:17 by vmazurok          #+#    #+#             */
/*   Updated: 2018/05/23 20:08:19 by vmazurok         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"





//----------------------------------------------------------------------



void		add_obj(t_obj **ol, void *obj, char name)
{
	t_obj 	*step;
	t_obj 	*last;

	last = (t_obj *)malloc(sizeof(t_obj));
	last->name = name;
	last->obj = obj;
	last->next = NULL;
	step = *ol;
	if (step)
	{
		while (step->next)
			step = step->next;
		step->next = last;
	}
	else
		*ol = last;
}

void		add_light(t_light **light_list, t_light *light)
{
	t_light *step;
	// t_light *last;

	// last = (t_light *)malloc(sizeof(t_light));
	// last->direct = dir;
	// last->intence = intence;
	// last->is_dir = is_dir;
	// last->color = color;
	light->next = NULL;
	step = *light_list;
	if (step)
	{
		while (step->next)
			step = step->next;
		step->next = light;
	}
	else
		*light_list = light;
}

// t_vector	set_vector(double x, double y, double z)
// {
// 	t_vector v;

// 	v.x = x;
// 	v.y = y;
// 	v.z = z;
// 	return (v);
// }

void		error_caster(int line_number, char *s1, char *s2)
{
	ft_putstr("error in line ");
	ft_putnbr(line_number);
	ft_putstr("\n");
	ft_putstr(s1);
	ft_putstr("\"");
	ft_putstr(s2);
	ft_putendl("\"");
	ft_strdel(&s2);
	// system("leaks a.out");
	exit(1);
}


int			begin_with(char *line, char *begin)
{
	int i;
	int j;

	i = 0;
	if (line && begin)
		while (begin[i])
		{
			if (!line[i])
				return (0);
			if (line[i] != begin[i])
				return (0);
			i++;
		}
	else
		return (0);
	return (1);
}

char		*trim_from(char *line, int i)
{
	char *new_line;

	if (!line)
		exit(1);
	if (ft_strlen(line) >= i)
		while (line[i] && line[i] < 33)
			i++;
	else
		i = 0;
	new_line = ft_strdup(line + i);
	ft_strdel(&line);
	return (new_line);
}

double		str_to_double(char *line, int i, int l_num)
{
	// int		i;
	int		len;
	double	num_whole;
	double	negative;
	double	num_drob;

	// i = 0;
	// printf("line in str_lo line |%s|, %i\n", line, i);
	if (!line)
		return 0;
	negative = 1;
	if (line[i] == '-')
	{
		negative = -1;
		i++;
	}
	if (!ft_isdigit(line[i]))
		error_caster(l_num, "wrong float number representation ", line);
	num_whole = (double)ft_atoi(line + i);
	while (ft_isdigit(line[i]))
		i++;
	if (line[i++] == '.')
	{
		if (!ft_isdigit(line[i]))
			error_caster(l_num, "wrong float number representation ", line);
		len = 0;
		num_drob = (double)ft_atoi(line + i);
		while (ft_isdigit(line[i++]))
			len++;
		num_drob = num_drob / pow(10, len);
		num_whole += num_drob;
	}
	return (num_whole * negative);
}

int			parce_color(char *line, int l_num)
{
	int 		i;
	int			j;
	int 		col;
	const char	base[16] = "0123456789ABCDEF";

	i = 1;
	col = 0;
	if (begin_with(line, "0x"))
	{
		while (line[++i])
		{
			j = 0;
			while (base[j] && base[j] != ft_toupper(line[i]))
				j++;
			if (!base[j])
				error_caster(l_num, "wrong color representation ", line);
			col = col * 16 + j;
		}
		return (col);
	}
	error_caster(l_num, "wrong color representation ", line);
	return (0);
}

t_vector	parce_vector(char *line, int l_num)
{
	int i;
	int flag;
	double a;
	double b;
	double c;

	i = 1;
	flag = 0;
	if (begin_with(line, "("))
	{
		a = str_to_double(line, i, l_num);
		while (line[i] && (line[i] != ',' || ft_isdigit(line[i]) || line[i] == '.'))
			i++;
		while (line[++i] && line[i] < 33)
			;
		b = str_to_double(line, i, l_num);
		while (line[i] && (line[i] != ',' || ft_isdigit(line[i]) || line[i] == '.'))
			i++;
		while (line[++i] && line[i] < 33)
			;
		c = str_to_double(line, i, l_num);
		while (line[i])
		{
			if (line[i] == ')')
				return (set_vector(a, b, c));
			i++;
		}
	}
	error_caster(l_num, "wrong vector format ", line);
	return (set_vector(0, 0, 0));
}

int			cam_parce(int fd, int *l_num, t_camera *camera)
{
	int			k;
	int			flag;
	char		*line;
	char		*new_line;

	flag = 0;
	while ((k = get_next_line(fd, &line)) > 0)
	{
		(*l_num)++;
		new_line = ft_strtrim(line);
		ft_strdel(&line);
		line = new_line;
		if (begin_with(line, "dir:"))
		{
			line = trim_from(line, 4);
			(*camera).direct = v_to_len(parce_vector(line, *l_num), 1, 0);
			flag = flag | 1;
			// printf("directory of (*camera) %f, %f, %f\n", (*camera).direct.x, (*camera).direct.y, (*camera).direct.z);
		}
		else if (begin_with(line, "cen:"))
		{
			line = trim_from(line, 4);
			(*camera).origin = parce_vector(line, *l_num);
			flag = flag | 2;
			// printf("centre of (*camera) %f, %f, %f\n", (*camera).origin.x, (*camera).origin.y, (*camera).origin.z);
		}
		else
			error_caster(*l_num, "no such parameter as ", line);
		ft_strdel(&line);
		if (flag == 3)
			break ;
	}
	if (k < 0)
	{
		perror("rt_v1");
		exit(1);
	}
	(*camera).is_set = 1;
	return (0);
}

int			light_parce(int fd, int *l_num, void **light_list, int n)
{
	int			flag;
	char		*line;
	char		*new_line;
	int			k;
	double		intence;
	t_light		*light;

	flag = 0; //p-d-ce/dir-col-int
	light = (t_light *)malloc(sizeof(t_light));
	n++;
	while ((k = get_next_line(fd, &line)) > 0)
	{
		(*l_num)++;
		new_line = ft_strtrim(line);
		ft_strdel(&line);
		line = new_line;
		if (ft_strequ(line, "point"))
		{
			// printf("find point\n");
			light->is_dir = 0;
			flag = flag & ~(1 << 3);
			flag = flag | (1 << 4);
		}
		else if (ft_strequ(line, "direct"))
		{
			// printf("find direct\n");
			light->is_dir = 1;
			flag = flag & ~(1 << 4);
			flag = flag | (1 << 3);
		}
		else if ((begin_with(line, "dir:") && (flag & (1 << 3))) || (begin_with(line, "cen:") && (flag & (1 << 4))))
		{
			// printf("find dir: or cen:\n");
			line = trim_from(line, 4);
			light->direct = parce_vector(line, *l_num);
			flag = flag | (1 << 2);
		}
		else if (begin_with(line, "col:"))
		{
			// printf("find col:\n");
			line = trim_from(line, 4);
			light->color = parce_color(line, *l_num);
			flag = flag | (1 << 1);
		}
		else if (begin_with(line, "int:"))
		{
			// printf("find int:\n");
			line = trim_from(line, 4);
			flag = flag | 1;
			intence = str_to_double(line, 0, *l_num);
			if (intence > 1 || intence < 0)
				error_caster(*l_num, "no such intence as ", line);
			light->intence = intence;
		}
		else
		{
			free(light);
			error_caster(*l_num, "no such parameter as ", line);
		}
		ft_strdel(&line);
		if (flag == 15 || flag == 23)
			break ;
	}
	if (k < 0)
	{
		perror("rt_v1");
		exit(1);
	}
	add_light((t_light **)light_list, light);
	return (0);
}

int			sphere_parce(int fd, int *l_num, void **shape_list, int id)
{
	int			k;
	int			flag;
	char		*line;
	char		*new_line;
	t_sphere	*sphere;

	flag = 0;
	sphere = (t_sphere *)malloc(sizeof(t_sphere));
	sphere->id = id;
	while ((k = get_next_line(fd, &line)) > 0)
	{
		(*l_num)++;
		new_line = ft_strtrim(line);
		ft_strdel(&line);
		line = new_line;
		if (begin_with(line, "cen:"))
		{
			line = trim_from(line, 4);
			sphere->centre = parce_vector(line, *l_num);
			flag = flag | 1;
		}
		else if (begin_with(line, "col:"))
		{
			line = trim_from(line, 4);
			sphere->color = parce_color(line, *l_num);
			flag = flag | 2;
		}
		else if (begin_with(line, "rad:"))
		{
			line = trim_from(line, 4);
			sphere->radius = str_to_double(line, 0, *l_num);
			flag = flag | 4;
		}
		else
			error_caster(*l_num, "no such parameter as ", line);
		ft_strdel(&line);
		if (flag == 7)
			break ;
	}
	if (k < 0)
	{
		perror("rt_v1");
		exit(1);
	}
	add_shape((t_shape **)shape_list, sphere, 's', id);
	return (0);
}

int			plane_parce(int fd, int *l_num, void **shape_list, int id)
{
	int			k;
	int			flag;
	char		*line;
	char		*new_line;
	t_plane		*plane;

	flag = 0;
	plane = (t_plane *)malloc(sizeof(t_plane));
	plane->id = id;
	while ((k = get_next_line(fd, &line)) > 0)
	{
		(*l_num)++;
		new_line = ft_strtrim(line);
		ft_strdel(&line);
		line = new_line;
		if (begin_with(line, "cen:"))
		{
			line = trim_from(line, 4);
			plane->point = parce_vector(line, *l_num);
			flag = flag | 1;
		}
		else if (begin_with(line, "col:"))
		{
			line = trim_from(line, 4);
			plane->color = parce_color(line, *l_num);
			flag = flag | 2;
		}
		else if (begin_with(line, "nor:"))
		{
			line = trim_from(line, 4);
			plane->normal = v_to_len(parce_vector(line, *l_num), 1, 0);
			flag = flag | 4;
		}
		else
			error_caster(*l_num, "no such parameter as ", line);
		ft_strdel(&line);
		if (flag == 7)
			break ;
	}
	if (k < 0)
	{
		perror("rt_v1");
		exit(1);
	}
	add_shape((t_shape **)shape_list, plane, 'p', id);
	return (0);
}

int			cone_parce(int fd, int *l_num, void **shape_list, int id)
{
	int			k;
	int			flag;
	char		*line;
	char		*new_line;
	t_cone		*cone;

	flag = 0;
	cone = (t_cone *)malloc(sizeof(t_cone));
	cone->id = id;
	while ((k = get_next_line(fd, &line)) > 0)
	{
		(*l_num)++;
		new_line = ft_strtrim(line);
		ft_strdel(&line);
		line = new_line;
		if (begin_with(line, "cen:"))
		{
			line = trim_from(line, 4);
			cone->point = parce_vector(line, *l_num);
			flag = flag | 1;
		}
		else if (begin_with(line, "col:"))
		{
			line = trim_from(line, 4);
			cone->color = parce_color(line, *l_num);
			flag = flag | 2;
		}
		else if (begin_with(line, "dir:"))
		{
			line = trim_from(line, 4);
			cone->direct = v_to_len(parce_vector(line, *l_num), 1, 0);
			flag = flag | (1 << 2);
		}
		else if (begin_with(line, "ang:"))
		{
			line = trim_from(line, 4);
			cone->ang = fmin(1, fmax(0.05, tan(str_to_double(line, 0, *l_num) *
			M_PI / 180)));
			flag = flag | (1 << 3);
		}
		else
			error_caster(*l_num, "no such parameter as ", line);
		ft_strdel(&line);
		if (flag == 15)
			break ;
	}
	if (k < 0)
	{
		perror("rt_v1");
		exit(1);
	}
	add_shape((t_shape **)shape_list, cone, 'c', id);
	return (0);
}

int			cylin_parce(int fd, int *l_num, void **shape_list, int id)
{
	int			k;
	int			flag;
	char		*line;
	char		*new_line;
	t_cylinder	*cyl;

	flag = 0;
	cyl = (t_cylinder *)malloc(sizeof(t_cylinder));
	cyl->id = id;
	while ((k = get_next_line(fd, &line)) > 0)
	{
		(*l_num)++;
		new_line = ft_strtrim(line);
		ft_strdel(&line);
		line = new_line;
		if (begin_with(line, "cen:"))
		{
			line = trim_from(line, 4);
			cyl->point = parce_vector(line, *l_num);
			flag = flag | 1;
		}
		else if (begin_with(line, "col:"))
		{
			line = trim_from(line, 4);
			cyl->color = parce_color(line, *l_num);
			flag = flag | 2;
		}
		else if (begin_with(line, "dir:"))
		{
			line = trim_from(line, 4);
			cyl->direct = v_to_len(parce_vector(line, *l_num), 1, 0);
			flag = flag | (1 << 2);
		}
		else if (begin_with(line, "rad:"))
		{
			line = trim_from(line, 4);
			cyl->radius = str_to_double(line, 0, *l_num);
			flag = flag | (1 << 3);
		}
		else
			error_caster(*l_num, "no such parameter as ", line);
		ft_strdel(&line);
		if (flag == 15)
			break ;
	}
	if (k < 0)
	{
		perror("rt_v1");
		exit(1);
	}
	add_shape((t_shape **)shape_list, cyl, 0, id);
	return (0);
}

void		correct_plane_normal(t_shape *shape_list, t_camera camera)
{
	t_vector nor;

	while (shape_list)
	{
		if (shape_list->name == 'p')
		{
			nor = ((t_plane *)(shape_list->shape))->normal;
			if (scalar_dob(nor, camera.direct) <= 0)
				((t_plane *)(shape_list->shape))->normal = v_to_len(nor, -1, 0);
		}
		shape_list = shape_list->next;
	}
}

int			main(int argc, char **argv) //char *name
{
	int			fd;
	int			i;
	int			l_num;
	int			k;
	int			id;
	char		*line;
	char		*new_line;
	t_shape		*shape_list;
	t_camera	camera;
	t_light		*light;
	t_grafx		*gfx;

	shape_list = NULL;
	light = NULL;
	camera.is_set = 0;
	id = 0;
	t_parce arr[] =
	{
		{"camera:", NULL},
		{"light:", &light_parce},
		{"sphere:", &sphere_parce},
		{"plane:", &plane_parce},
		{"cone:", &cone_parce},
		{"cylinder:", &cylin_parce}
	};

	l_num = 0;
	if (argc != 2)
	{
		ft_putendl("Input must include one parameter");
		exit(1);
	}
	if ((fd = open(argv[1], O_RDONLY)) < 0)
	{
		perror("rt_v1");
		exit(1);
	}
	while ((k = get_next_line(fd, &line)) > 0)
	{
		l_num++;
		new_line = ft_strtrim(line);
		free(line);
		line = new_line;
		i = 0;
		while (i < OBJ_NUM)
		{
			if (*line == 0)
				break;
			else if (ft_strequ(line, arr[i].name))
			{
				if (ft_strequ(line, "camera:"))
					cam_parce(fd, &l_num, &camera);
				else if (ft_strequ(line, "light:"))
					arr[i].parce(fd, &l_num, (void **)&light, 0);
				else
					arr[i].parce(fd, &l_num, (void **)&shape_list, id++);
				break ;
			}
			i++;
			if (i == OBJ_NUM)
			{
				// printf("|%s|\n\n\n", line);
				
				error_caster(l_num, "no such object as ", line);
			}
		}
		ft_strdel(&line);
	}
	if (!camera.is_set)
	{
		ft_putendl("You have no camera, ma dudes");
		exit(1);
	}

	gfx = (t_grafx *)malloc(sizeof(t_grafx));
	if (!(gfx->mlx = mlx_init()))
	{
		perror("Error");
		exit(1);
	}
	if (!(gfx->win = mlx_new_window(gfx->mlx, SCR_SIZE, SCR_SIZE, "shit_RTv1")))
	{
		perror("Error");
		exit(1);
	}
	if (!(gfx->img = mlx_new_image(gfx->mlx, SCR_SIZE, SCR_SIZE)))
	{
		perror("Error");
		exit(1);
	}

	correct_plane_normal(shape_list, camera);
	drowing(gfx, shape_list, light, camera);
	mlx_loop(gfx->mlx);
	// system("leaks a.out");
	return (0);
}